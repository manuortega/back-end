# Test técnico Nodejs
## Cargo BackEnd - Duración 4 hrs



## Instrucciones:


Construir una API utilizando nodejs y expressjs, se solicita cumplir con los siguientes requerimientos:

1 . Realizar un fork del proyecto existente a un repositorio en su cuenta.


2 . Crear la siguiente estructura en la base de datos 

```javascript
{
    id: integer,
    name: string,
    email: string,
    password: string,
    status: string,
}
```

3 . Se debe administrar los siguientes datos de un usuario a través de Endpoint de la API a construir 

- Crear Usuarios nuevos
- Modificar usuarios existentes
- Eliminar datos de usuario
- Listar un usuario
- Listar todos los usuarios

4 . Generar documentación del uso de la API 



## Adquieres mayor puntuación si:

```

- Pruebas de stress a la API

- Utilizar Estandar REST

- Utilización de patrones de Diseño

- Utilizar Docker para implementar la infraestructura

- Utilizar base de datos postgreSql

- Utilizar base de datos Redis para mejorar el performance de la api

- Construir la API utilizando TypeScript

```




### ¿Tienes dudas? escríbenos a postulaciones@frenon.com

